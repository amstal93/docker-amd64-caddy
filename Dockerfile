FROM registry.gitlab.com/offtechnologies/docker-caddy-builder:latest as builder

ARG version="1.0.1"

RUN VERSION=${version} ENABLE_TELEMETRY=false /bin/sh /usr/bin/build.sh


FROM alpine:3.10.1

ARG version="1.0.1"
LABEL caddy_version="$version"
ENV ENABLE_TELEMETRY="false"

RUN apk add --no-cache openssh-client libcap

COPY --from=builder /install/caddy /usr/bin/caddy

RUN chmod 0755 /usr/bin/caddy && \
    addgroup -S caddy && \
    adduser -D -S -s /sbin/nologin -G caddy caddy && \
    setcap cap_net_bind_service=+ep `readlink -f /usr/bin/caddy` && \
    /usr/bin/caddy -version

EXPOSE 80 443 2015
VOLUME /srv
WORKDIR /srv

ADD config/Caddyfile /etc/Caddyfile
ADD config/index.html /srv/index.html

RUN chown -R caddy:caddy /srv

USER caddy

ENTRYPOINT ["/usr/bin/caddy"]
CMD ["--conf", "/etc/Caddyfile"]
