## docker-amd64-caddy

[![pipeline status](https://gitlab.com/offtechnologies/docker-amd64-caddy/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/docker-amd64-caddy/commits/master)
[![This image on DockerHub](https://img.shields.io/docker/pulls/offtechnologies/docker-amd64-caddy.svg)](https://hub.docker.com/r/offtechnologies/docker-amd64-caddy/)
[![](https://images.microbadger.com/badges/version/offtechnologies/docker-amd64-caddy.svg)](https://microbadger.com/images/offtechnologies/docker-amd64-caddy "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/offtechnologies/docker-amd64-caddy.svg)](https://microbadger.com/images/offtechnologies/docker-amd64-caddy "Get your own image badge on microbadger.com")


[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Caddy Web Server running on Alpine Linux (AMD64) in a Docker container.

> **Important Note**: Use on production servers at your own risk!

## How to Use

```bash
docker run -d \
    -v /path/to/my/index.html:/srv \
    -v path/to/my/Caddyfile:/etc/Caddyfile \
    -p 2015:2015 \
    registry.gitlab.com/offtechnologies/docker-amd64-caddy:master
```
Point your browser to http://host-ip:2015 and Enjoy!

## Credits:

[abiosoft](https://github.com/abiosoft/caddy-docker)

[Yoba Systems](https://github.com/yobasystems/alpine-caddy)
